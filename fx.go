package tasks

import (
	"context"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"go.saastack.io/jaal/schemabuilder"
	"go.saastack.io/task/pb"
	"go.uber.org/fx"
	"google.golang.org/grpc"
)

// Module is the fx module encapsulating all the providers of the package
var Module = fx.Options(
	fx.Provide(
		pb.NewPostgresTaskStore,
		pb.NewPostgresTaskActivityStore,

		NewTasksServer,
		fx.Annotated{
			Name: "public",
			Target: func(taskSrv pb.TasksServer, taskActSrv pb.TaskActivityStore) (pb.TasksServer, pb.TaskActivityStore) {
				return taskSrv, taskActSrv
			},
		},

		pb.NewLocalTasksClient,
		fx.Annotated{
			Name: "public",
			Target: func(in struct {
				fx.In
				S pb.TasksServer `name:"public"`
			}) pb.TasksClient {
				return pb.NewLocalTasksClient(in.S)
			},
		},

        NewParentServiceServer,
        pb.NewLocalParentServiceClient,

		fx.Annotated{
			Group:  "grpc-service",
			Target: RegisterGRPCService,
		},
		fx.Annotated{
			Group:  "graphql-service",
			Target: RegisterGraphQLService,
		},
		fx.Annotated{
			Group:  "http-service",
			Target: RegisterHttpService,
		},
	),
	fx.Decorate(
		pb.NewEventsTasksServer,
	),
)

func RegisterGRPCService(in struct {
	fx.In
	Server pb.TasksServer `name:"public"`
}) func(s *grpc.Server) {
	return func(s *grpc.Server) {
		pb.RegisterTasksServer(s, in.Server)
	}
}

func RegisterGraphQLService(in struct {
	fx.In
	Client pb.TasksClient `name:"public"`
}) func(s *schemabuilder.Schema) {
	return func(s *schemabuilder.Schema) {
		pb.RegisterTasksOperations(s, in.Client)
        //pb.RegisterTasksActivities(s, in.Client)
	}
}

func RegisterHttpService(in struct {
	fx.In
	Client pb.TasksClient `name:"public"`
}) func(*runtime.ServeMux, context.Context) error {
	return func(mux *runtime.ServeMux, ctx context.Context) error {
		return pb.RegisterTasksHandlerClient(ctx, mux, in.Client)
	}
}
