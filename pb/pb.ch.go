package pb

import (
	context "context"
	x "database/sql"

	sqrl "github.com/elgris/sqrl"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	chaku_globals "go.saastack.io/chaku/chaku-globals"
	driver "go.saastack.io/chaku/driver"
	sql "go.saastack.io/chaku/driver/pgsql"
	errors "go.saastack.io/chaku/errors"
)

var objectTableMap = chaku_globals.ObjectTable{
	"task": {
		"id":            "task",
		"title":         "task",
		"due_timestamp": "task",
		"send_notif":    "task",
		"notif_type":    "task",
		"assignee":      "task",
		"status":        "task",
		"high_prior":    "task",
	},
	"task_activity": {
		"id":      "task_activity",
		"task_id": "task_activity",
		"msg":     "task_activity",
	},
}

func (m *Task) PackageName() string {
	return "saastack_task_v1"
}

func (m *Task) TableOfObject(f, s string) string {
	return objectTableMap[f][s]
}

func (m *Task) GetDescriptorsOf(f string) (driver.Descriptor, error) {
	switch f {
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Task) ObjectName() string {
	return "task"
}

func (m *Task) Fields() []string {
	return []string{
		"id", "title", "due_timestamp", "send_notif", "notif_type", "assignee", "status", "high_prior",
	}
}

func (m *Task) IsObject(field string) bool {
	switch field {
	default:
		return false
	}
}

func (m *Task) ValuerSlice(field string) ([]driver.Descriptor, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	default:
		return []driver.Descriptor{}, errors.ErrInvalidField
	}
}

func (m *Task) Valuer(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return m.Id, nil
	case "title":
		return m.Title, nil
	case "due_timestamp":
		return m.DueTimestamp, nil
	case "send_notif":
		return m.SendNotif, nil
	case "notif_type":
		return m.NotifType, nil
	case "assignee":
		return m.Assignee, nil
	case "status":
		return m.Status, nil
	case "high_prior":
		return m.HighPrior, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Task) Addresser(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return &m.Id, nil
	case "title":
		return &m.Title, nil
	case "due_timestamp":
		return &m.DueTimestamp, nil
	case "send_notif":
		return &m.SendNotif, nil
	case "notif_type":
		return &m.NotifType, nil
	case "assignee":
		return &m.Assignee, nil
	case "status":
		return &m.Status, nil
	case "high_prior":
		return &m.HighPrior, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Task) New(field string) error {
	switch field {
	case "id":
		return nil
	case "title":
		return nil
	case "due_timestamp":
		if m.DueTimestamp == nil {
			m.DueTimestamp = &timestamp.Timestamp{}
		}
		return nil
	case "send_notif":
		return nil
	case "notif_type":
		return nil
	case "assignee":
		return nil
	case "status":
		return nil
	case "high_prior":
		return nil
	default:
		return errors.ErrInvalidField
	}
}

func (m *Task) Type(field string) string {
	switch field {
	case "id":
		return "string"
	case "title":
		return "string"
	case "due_timestamp":
		return "timestamp"
	case "send_notif":
		return "bool"
	case "notif_type":
		return "enum"
	case "assignee":
		return "string"
	case "status":
		return "enum"
	case "high_prior":
		return "bool"
	default:
		return ""
	}
}

func (_ *Task) GetEmptyObject() (m *Task) {
	m = &Task{}
	return
}

func (m *Task) GetPrefix() string {
	return "tas"
}

func (m *Task) GetID() string {
	return m.Id
}

func (m *Task) SetID(id string) {
	m.Id = id
}

func (m *Task) IsRoot() bool {
	return true
}

func (m *Task) IsFlatObject(f string) bool {
	return false
}

func (m *Task) NoOfParents(d driver.Descriptor) int {
	switch d.ObjectName() {
	}
	return 0
}

type TaskStore struct {
	d      driver.Driver
	withTx bool
	tx     driver.Transaction

	limitMultiplier int
}

func (s TaskStore) Execute(ctx context.Context, query string, args ...interface{}) error {
	if s.withTx {
		return s.tx.Execute(ctx, query, args...)
	}
	return s.d.Execute(ctx, query, args...)
}

func (s TaskStore) QueryRows(ctx context.Context, query string, scanners []string, args ...interface{}) (driver.Result, error) {
	if s.withTx {
		return s.tx.QueryRows(ctx, query, scanners, args...)
	}
	return s.d.QueryRows(ctx, query, scanners, args...)
}

func NewTaskStore(d driver.Driver) TaskStore {
	return TaskStore{d: d, limitMultiplier: 1}
}

func NewPostgresTaskStore(db *x.DB, usr driver.IUserInfo) TaskStore {
	return TaskStore{
		d:               &sql.Sql{DB: db, UserInfo: usr, Placeholder: sqrl.Dollar},
		limitMultiplier: 1,
	}
}

type TaskTx struct {
	TaskStore
}

func (s TaskStore) BeginTx(ctx context.Context) (*TaskTx, error) {
	tx, err := s.d.BeginTx(ctx)
	if err != nil {
		return nil, err
	}
	return &TaskTx{
		TaskStore: TaskStore{
			d:      s.d,
			withTx: true,
			tx:     tx,
		},
	}, nil
}

func (tx *TaskTx) Commit(ctx context.Context) error {
	return tx.tx.Commit(ctx)
}

func (tx *TaskTx) RollBack(ctx context.Context) error {
	return tx.tx.RollBack(ctx)
}

func (s TaskStore) CreateTaskPGStore(ctx context.Context) error {
	const queries = `
CREATE SCHEMA IF NOT EXISTS saastack_task_v1;
CREATE TABLE IF NOT EXISTS  saastack_task_v1.task( id text DEFAULT ''::text , title text DEFAULT ''::text , due_timestamp timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone , send_notif boolean DEFAULT false , notif_type integer DEFAULT 0 , assignee text DEFAULT ''::text , status integer DEFAULT 0 , high_prior boolean DEFAULT false , parent text DEFAULT ''::text , is_deleted boolean DEFAULT false, deleted_by text DEFAULT ''::text, deleted_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, updated_by text DEFAULT ''::text, updated_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, created_by text DEFAULT ''::text, created_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, field_variable_mask text DEFAULT ''::text, PRIMARY KEY (id, parent)); 
CREATE TABLE IF NOT EXISTS  saastack_task_v1.task_parent( id text DEFAULT ''::text , parent text DEFAULT ''::text ); 
`
	if err := s.d.Execute(ctx, queries); err != nil {
		return err
	}
	return nil
}

func (s TaskStore) CreateTasks(ctx context.Context, list ...*Task) ([]string, error) {
	vv := make([]driver.Descriptor, len(list))
	for i := range list {
		vv[i] = list[i]
	}
	if s.withTx {
		return s.tx.Insert(ctx, vv, &Task{}, &Task{}, "", []string{})
	}
	return s.d.Insert(ctx, vv, &Task{}, &Task{}, "", []string{})
}

func (s TaskStore) DeleteTask(ctx context.Context, cond TaskCondition) error {
	if s.withTx {
		return s.tx.Delete(ctx, cond.taskCondToDriverTaskCond(s.d), &Task{}, &Task{})
	}
	return s.d.Delete(ctx, cond.taskCondToDriverTaskCond(s.d), &Task{}, &Task{})
}

func (s TaskStore) UpdateTask(ctx context.Context, req *Task, fields []string, cond TaskCondition) error {
	if s.withTx {
		return s.tx.Update(ctx, cond.taskCondToDriverTaskCond(s.d), req, &Task{}, fields...)
	}
	return s.d.Update(ctx, cond.taskCondToDriverTaskCond(s.d), req, &Task{}, fields...)
}

func (s TaskStore) UpdateTaskMetaInfo(ctx context.Context, list ...*driver.UpdateMetaInfoRequest) error {
	fn := s.d.UpdateMetaInfo
	if s.withTx {
		fn = s.tx.UpdateMetaInfo
	}
	return fn(ctx, &Task{}, &Task{}, list...)
}

func (s TaskStore) GetTask(ctx context.Context, fields []string, cond TaskCondition, opt ...getTasksOption) (*Task, error) {
	if len(fields) == 0 {
		fields = (&Task{}).Fields()
	}
	m := MetaInfoForList{}
	listOpts := []listTasksOption{
		&CursorBasedPagination{Limit: 1},
	}
	for _, o := range opt {
		t, _ := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			listOpts = append(listOpts, &m)
		}
	}
	objList, err := s.ListTasks(ctx, fields, cond, listOpts...)
	if len(objList) == 0 && err == nil {
		err = errors.ErrNotFound
	}
	if err != nil {
		return nil, err
	}
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			in.(*MetaInfo).UpdatedBy = m[0].UpdatedBy
			in.(*MetaInfo).CreatedBy = m[0].CreatedBy
			in.(*MetaInfo).DeletedBy = m[0].DeletedBy
			in.(*MetaInfo).UpdatedOn = m[0].UpdatedOn
			in.(*MetaInfo).CreatedOn = m[0].CreatedOn
			in.(*MetaInfo).DeletedOn = m[0].DeletedOn
			in.(*MetaInfo).IsDeleted = m[0].IsDeleted
		}
	}
	return objList[0], nil
}

func (s TaskStore) ListTasks(ctx context.Context, fields []string, cond TaskCondition, opt ...listTasksOption) ([]*Task, error) {
	if len(fields) == 0 {
		fields = (&Task{}).Fields()
	}
	var (
		res driver.Result
		err error
		m   driver.MetaInfo

		limit       = -1
		orderByList = make([]driver.OrderByType, 0, 5)
	)
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			page, ok := in.(*CursorBasedPagination)
			if page != nil && ok {
				if page.SetTaskCondition == nil {
					page.SetTaskCondition = defaultSetTaskCondition
				}
				cond = page.SetTaskCondition(page.UpOrDown, page.Cursor, cond)
				limit = page.Limit + 1
				if len(orderByList) == 0 {
					orderByList = append(orderByList, driver.OrderByType{
						Field:     "id",
						Ascending: !page.UpOrDown,
					})
				}
			}
		case driver.OptionType_MetaInfo:
			s.d.MetaInfoRequested(&ctx, &m)
		case driver.OptionType_OrderBy:
			by, ok := in.(OrderBy)
			if ok && len(by.Bys) != 0 {
				orderByList = by.Bys
			}
		}
	}
	if len(orderByList) == 0 {
		orderByList = append(orderByList, driver.OrderByType{
			Field:     "id",
			Ascending: true,
		})
	}
	ctx = driver.SetOrderBy(ctx, orderByList...)
	if limit > 0 {
		ctx = driver.SetListLimit(ctx, limit)
	}

	if s.withTx {
		res, err = s.tx.Get(ctx, cond.taskCondToDriverTaskCond(s.d), &Task{}, &Task{}, fields...)
	} else {
		res, err = s.d.Get(ctx, cond.taskCondToDriverTaskCond(s.d), &Task{}, &Task{}, fields...)
	}
	if err != nil {
		return nil, err
	}
	defer res.Close()

	mp := map[string]struct{}{}
	list := make([]*Task, 0, 1000)
	infoMap := make(map[string]*driver.MetaInfo, 0)

	for res.Next(ctx) && limit != 0 {
		obj := &Task{}
		if err := res.Scan(ctx, obj); err != nil {
			return nil, err
		}
		for _, o := range opt {
			t, _ := o.getValue()
			switch t {
			case driver.OptionType_MetaInfo:
				infoMap[obj.Id] = &driver.MetaInfo{
					UpdatedBy: m.UpdatedBy,
					CreatedBy: m.CreatedBy,
					DeletedBy: m.DeletedBy,
					UpdatedOn: m.UpdatedOn,
					CreatedOn: m.CreatedOn,
					DeletedOn: m.DeletedOn,
					IsDeleted: m.IsDeleted,
				}
				break
			}
		}
		list = append(list, obj)
		if _, ok := mp[obj.Id]; !ok {
			limit--
			mp[obj.Id] = struct{}{}
		}
	}
	if err := res.Close(); err != nil {
		return nil, err
	}

	list = MapperTask(list)
	meta := &MetaInfoForList{}

	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			page, ok := in.(*CursorBasedPagination)
			if page != nil && ok {
				if len(list) <= page.Limit {
					page.HasNext = false
					page.HasPrevious = false
				} else {
					list = list[:page.Limit]
					if page.UpOrDown {
						page.HasPrevious = true
					} else {
						page.HasNext = true
					}
				}
			}
		case driver.OptionType_MetaInfo:
			meta = in.(*MetaInfoForList)
		}
	}
	for _, l := range list {
		*meta = append(*meta, infoMap[l.Id])
	}
	return list, nil
}

func (s TaskStore) CountTasks(ctx context.Context, cond TaskCondition) (int, error) {
	cntFn := s.d.Count
	if s.withTx {
		cntFn = s.tx.Count
	}
	return cntFn(ctx, cond.taskCondToDriverTaskCond(s.d), &Task{}, &Task{})
}

type getTasksOption interface {
	getOptTasks() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfo) getOptTasks() { // method of no significant use
}

type listTasksOption interface {
	listOptTasks() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfoForList) listOptTasks() {
}

func (OrderBy) listOptTasks() {
}

func (*CursorBasedPagination) listOptTasks() {
}

func defaultSetTaskCondition(upOrDown bool, cursor string, cond TaskCondition) TaskCondition {
	if upOrDown {
		if cursor != "" {
			return TaskAnd{cond, TaskIdLt{cursor}}
		}
		return cond
	}
	if cursor != "" {
		return TaskAnd{cond, TaskIdGt{cursor}}
	}
	return cond
}

type TaskAnd []TaskCondition

func (p TaskAnd) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.taskCondToDriverTaskCond(d))
	}
	return driver.And{Conditioners: dc, Operator: d}
}

type TaskOr []TaskCondition

func (p TaskOr) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.taskCondToDriverTaskCond(d))
	}
	return driver.Or{Conditioners: dc, Operator: d}
}

type TaskParentEq struct {
	Parent string
}

func (c TaskParentEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskFullParentEq struct {
	Parent string
}

func (c TaskFullParentEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskParentNotEq struct {
	Parent string
}

func (c TaskParentNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskFullParentNotEq struct {
	Parent string
}

func (c TaskFullParentNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskParentLike struct {
	Parent string
}

func (c TaskParentLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskFullParentLike struct {
	Parent string
}

func (c TaskFullParentLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskParentILike struct {
	Parent string
}

func (c TaskParentILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskFullParentILike struct {
	Parent string
}

func (c TaskFullParentILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskParentIn struct {
	Parent []string
}

func (c TaskParentIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskFullParentIn struct {
	Parent []string
}

func (c TaskFullParentIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskParentNotIn struct {
	Parent []string
}

func (c TaskParentNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskFullParentNotIn struct {
	Parent []string
}

func (c TaskFullParentNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdEq struct {
	Id string
}

func (c TaskIdEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleEq struct {
	Title string
}

func (c TaskTitleEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueTimestampEq struct {
	DueTimestamp *timestamp.Timestamp
}

func (c TaskDueTimestampEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "due_timestamp", Value: c.DueTimestamp, Operator: d, Descriptor: &Task{}, FieldMask: "due_timestamp", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskSendNotifEq struct {
	SendNotif bool
}

func (c TaskSendNotifEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "send_notif", Value: c.SendNotif, Operator: d, Descriptor: &Task{}, FieldMask: "send_notif", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskNotifTypeEq struct {
	NotifType NotifType
}

func (c TaskNotifTypeEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "notif_type", Value: c.NotifType, Operator: d, Descriptor: &Task{}, FieldMask: "notif_type", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeEq struct {
	Assignee string
}

func (c TaskAssigneeEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "assignee", Value: c.Assignee, Operator: d, Descriptor: &Task{}, FieldMask: "assignee", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStatusEq struct {
	Status Status
}

func (c TaskStatusEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "status", Value: c.Status, Operator: d, Descriptor: &Task{}, FieldMask: "status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskHighPriorEq struct {
	HighPrior bool
}

func (c TaskHighPriorEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "high_prior", Value: c.HighPrior, Operator: d, Descriptor: &Task{}, FieldMask: "high_prior", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdNotEq struct {
	Id string
}

func (c TaskIdNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleNotEq struct {
	Title string
}

func (c TaskTitleNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueTimestampNotEq struct {
	DueTimestamp *timestamp.Timestamp
}

func (c TaskDueTimestampNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "due_timestamp", Value: c.DueTimestamp, Operator: d, Descriptor: &Task{}, FieldMask: "due_timestamp", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskSendNotifNotEq struct {
	SendNotif bool
}

func (c TaskSendNotifNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "send_notif", Value: c.SendNotif, Operator: d, Descriptor: &Task{}, FieldMask: "send_notif", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskNotifTypeNotEq struct {
	NotifType NotifType
}

func (c TaskNotifTypeNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "notif_type", Value: c.NotifType, Operator: d, Descriptor: &Task{}, FieldMask: "notif_type", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeNotEq struct {
	Assignee string
}

func (c TaskAssigneeNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "assignee", Value: c.Assignee, Operator: d, Descriptor: &Task{}, FieldMask: "assignee", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStatusNotEq struct {
	Status Status
}

func (c TaskStatusNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "status", Value: c.Status, Operator: d, Descriptor: &Task{}, FieldMask: "status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskHighPriorNotEq struct {
	HighPrior bool
}

func (c TaskHighPriorNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "high_prior", Value: c.HighPrior, Operator: d, Descriptor: &Task{}, FieldMask: "high_prior", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdGt struct {
	Id string
}

func (c TaskIdGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleGt struct {
	Title string
}

func (c TaskTitleGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueTimestampGt struct {
	DueTimestamp *timestamp.Timestamp
}

func (c TaskDueTimestampGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "due_timestamp", Value: c.DueTimestamp, Operator: d, Descriptor: &Task{}, FieldMask: "due_timestamp", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskSendNotifGt struct {
	SendNotif bool
}

func (c TaskSendNotifGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "send_notif", Value: c.SendNotif, Operator: d, Descriptor: &Task{}, FieldMask: "send_notif", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskNotifTypeGt struct {
	NotifType NotifType
}

func (c TaskNotifTypeGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "notif_type", Value: c.NotifType, Operator: d, Descriptor: &Task{}, FieldMask: "notif_type", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeGt struct {
	Assignee string
}

func (c TaskAssigneeGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "assignee", Value: c.Assignee, Operator: d, Descriptor: &Task{}, FieldMask: "assignee", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStatusGt struct {
	Status Status
}

func (c TaskStatusGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "status", Value: c.Status, Operator: d, Descriptor: &Task{}, FieldMask: "status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskHighPriorGt struct {
	HighPrior bool
}

func (c TaskHighPriorGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "high_prior", Value: c.HighPrior, Operator: d, Descriptor: &Task{}, FieldMask: "high_prior", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdLt struct {
	Id string
}

func (c TaskIdLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleLt struct {
	Title string
}

func (c TaskTitleLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueTimestampLt struct {
	DueTimestamp *timestamp.Timestamp
}

func (c TaskDueTimestampLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "due_timestamp", Value: c.DueTimestamp, Operator: d, Descriptor: &Task{}, FieldMask: "due_timestamp", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskSendNotifLt struct {
	SendNotif bool
}

func (c TaskSendNotifLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "send_notif", Value: c.SendNotif, Operator: d, Descriptor: &Task{}, FieldMask: "send_notif", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskNotifTypeLt struct {
	NotifType NotifType
}

func (c TaskNotifTypeLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "notif_type", Value: c.NotifType, Operator: d, Descriptor: &Task{}, FieldMask: "notif_type", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeLt struct {
	Assignee string
}

func (c TaskAssigneeLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "assignee", Value: c.Assignee, Operator: d, Descriptor: &Task{}, FieldMask: "assignee", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStatusLt struct {
	Status Status
}

func (c TaskStatusLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "status", Value: c.Status, Operator: d, Descriptor: &Task{}, FieldMask: "status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskHighPriorLt struct {
	HighPrior bool
}

func (c TaskHighPriorLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "high_prior", Value: c.HighPrior, Operator: d, Descriptor: &Task{}, FieldMask: "high_prior", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdGtOrEq struct {
	Id string
}

func (c TaskIdGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleGtOrEq struct {
	Title string
}

func (c TaskTitleGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueTimestampGtOrEq struct {
	DueTimestamp *timestamp.Timestamp
}

func (c TaskDueTimestampGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "due_timestamp", Value: c.DueTimestamp, Operator: d, Descriptor: &Task{}, FieldMask: "due_timestamp", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskSendNotifGtOrEq struct {
	SendNotif bool
}

func (c TaskSendNotifGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "send_notif", Value: c.SendNotif, Operator: d, Descriptor: &Task{}, FieldMask: "send_notif", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskNotifTypeGtOrEq struct {
	NotifType NotifType
}

func (c TaskNotifTypeGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "notif_type", Value: c.NotifType, Operator: d, Descriptor: &Task{}, FieldMask: "notif_type", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeGtOrEq struct {
	Assignee string
}

func (c TaskAssigneeGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "assignee", Value: c.Assignee, Operator: d, Descriptor: &Task{}, FieldMask: "assignee", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStatusGtOrEq struct {
	Status Status
}

func (c TaskStatusGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "status", Value: c.Status, Operator: d, Descriptor: &Task{}, FieldMask: "status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskHighPriorGtOrEq struct {
	HighPrior bool
}

func (c TaskHighPriorGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "high_prior", Value: c.HighPrior, Operator: d, Descriptor: &Task{}, FieldMask: "high_prior", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdLtOrEq struct {
	Id string
}

func (c TaskIdLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleLtOrEq struct {
	Title string
}

func (c TaskTitleLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueTimestampLtOrEq struct {
	DueTimestamp *timestamp.Timestamp
}

func (c TaskDueTimestampLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "due_timestamp", Value: c.DueTimestamp, Operator: d, Descriptor: &Task{}, FieldMask: "due_timestamp", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskSendNotifLtOrEq struct {
	SendNotif bool
}

func (c TaskSendNotifLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "send_notif", Value: c.SendNotif, Operator: d, Descriptor: &Task{}, FieldMask: "send_notif", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskNotifTypeLtOrEq struct {
	NotifType NotifType
}

func (c TaskNotifTypeLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "notif_type", Value: c.NotifType, Operator: d, Descriptor: &Task{}, FieldMask: "notif_type", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeLtOrEq struct {
	Assignee string
}

func (c TaskAssigneeLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "assignee", Value: c.Assignee, Operator: d, Descriptor: &Task{}, FieldMask: "assignee", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStatusLtOrEq struct {
	Status Status
}

func (c TaskStatusLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "status", Value: c.Status, Operator: d, Descriptor: &Task{}, FieldMask: "status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskHighPriorLtOrEq struct {
	HighPrior bool
}

func (c TaskHighPriorLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "high_prior", Value: c.HighPrior, Operator: d, Descriptor: &Task{}, FieldMask: "high_prior", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdLike struct {
	Id string
}

func (c TaskIdLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleLike struct {
	Title string
}

func (c TaskTitleLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeLike struct {
	Assignee string
}

func (c TaskAssigneeLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "assignee", Value: c.Assignee, Operator: d, Descriptor: &Task{}, FieldMask: "assignee", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdILike struct {
	Id string
}

func (c TaskIdILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleILike struct {
	Title string
}

func (c TaskTitleILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeILike struct {
	Assignee string
}

func (c TaskAssigneeILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "assignee", Value: c.Assignee, Operator: d, Descriptor: &Task{}, FieldMask: "assignee", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeleted struct {
	IsDeleted bool
}

func (c TaskDeleted) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: c.IsDeleted, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByEq struct {
	By string
}

func (c TaskCreatedByEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedOnEq struct {
	On *timestamp.Timestamp
}

func (c TaskCreatedOnEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByNotEq struct {
	By string
}

func (c TaskCreatedByNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c TaskCreatedOnNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByGt struct {
	By string
}

func (c TaskCreatedByGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedOnGt struct {
	On *timestamp.Timestamp
}

func (c TaskCreatedOnGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByLt struct {
	By string
}

func (c TaskCreatedByLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedOnLt struct {
	On *timestamp.Timestamp
}

func (c TaskCreatedOnLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByGtOrEq struct {
	By string
}

func (c TaskCreatedByGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskCreatedOnGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByLtOrEq struct {
	By string
}

func (c TaskCreatedByLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskCreatedOnLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByLike struct {
	By string
}

func (c TaskCreatedByLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskCreatedByILike struct {
	By string
}

func (c TaskCreatedByILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByEq struct {
	By string
}

func (c TaskUpdatedByEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedOnEq struct {
	On *timestamp.Timestamp
}

func (c TaskUpdatedOnEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByNotEq struct {
	By string
}

func (c TaskUpdatedByNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c TaskUpdatedOnNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByGt struct {
	By string
}

func (c TaskUpdatedByGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedOnGt struct {
	On *timestamp.Timestamp
}

func (c TaskUpdatedOnGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByLt struct {
	By string
}

func (c TaskUpdatedByLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedOnLt struct {
	On *timestamp.Timestamp
}

func (c TaskUpdatedOnLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByGtOrEq struct {
	By string
}

func (c TaskUpdatedByGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskUpdatedOnGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByLtOrEq struct {
	By string
}

func (c TaskUpdatedByLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskUpdatedOnLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByLike struct {
	By string
}

func (c TaskUpdatedByLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskUpdatedByILike struct {
	By string
}

func (c TaskUpdatedByILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByEq struct {
	By string
}

func (c TaskDeletedByEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedOnEq struct {
	On *timestamp.Timestamp
}

func (c TaskDeletedOnEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByNotEq struct {
	By string
}

func (c TaskDeletedByNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c TaskDeletedOnNotEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByGt struct {
	By string
}

func (c TaskDeletedByGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedOnGt struct {
	On *timestamp.Timestamp
}

func (c TaskDeletedOnGt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByLt struct {
	By string
}

func (c TaskDeletedByLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedOnLt struct {
	On *timestamp.Timestamp
}

func (c TaskDeletedOnLt) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByGtOrEq struct {
	By string
}

func (c TaskDeletedByGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskDeletedOnGtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByLtOrEq struct {
	By string
}

func (c TaskDeletedByLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskDeletedOnLtOrEq) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByLike struct {
	By string
}

func (c TaskDeletedByLike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDeletedByILike struct {
	By string
}

func (c TaskDeletedByILike) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Task{}, RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdIn struct {
	Id []string
}

func (c TaskIdIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleIn struct {
	Title []string
}

func (c TaskTitleIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueTimestampIn struct {
	DueTimestamp []*timestamp.Timestamp
}

func (c TaskDueTimestampIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "due_timestamp", Value: c.DueTimestamp, Operator: d, Descriptor: &Task{}, FieldMask: "due_timestamp", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskSendNotifIn struct {
	SendNotif []bool
}

func (c TaskSendNotifIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "send_notif", Value: c.SendNotif, Operator: d, Descriptor: &Task{}, FieldMask: "send_notif", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskNotifTypeIn struct {
	NotifType []NotifType
}

func (c TaskNotifTypeIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "notif_type", Value: c.NotifType, Operator: d, Descriptor: &Task{}, FieldMask: "notif_type", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeIn struct {
	Assignee []string
}

func (c TaskAssigneeIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "assignee", Value: c.Assignee, Operator: d, Descriptor: &Task{}, FieldMask: "assignee", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStatusIn struct {
	Status []Status
}

func (c TaskStatusIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "status", Value: c.Status, Operator: d, Descriptor: &Task{}, FieldMask: "status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskHighPriorIn struct {
	HighPrior []bool
}

func (c TaskHighPriorIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "high_prior", Value: c.HighPrior, Operator: d, Descriptor: &Task{}, FieldMask: "high_prior", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskIdNotIn struct {
	Id []string
}

func (c TaskIdNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "id", Value: c.Id, Operator: d, Descriptor: &Task{}, FieldMask: "id", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskTitleNotIn struct {
	Title []string
}

func (c TaskTitleNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "title", Value: c.Title, Operator: d, Descriptor: &Task{}, FieldMask: "title", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskDueTimestampNotIn struct {
	DueTimestamp []*timestamp.Timestamp
}

func (c TaskDueTimestampNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "due_timestamp", Value: c.DueTimestamp, Operator: d, Descriptor: &Task{}, FieldMask: "due_timestamp", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskSendNotifNotIn struct {
	SendNotif []bool
}

func (c TaskSendNotifNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "send_notif", Value: c.SendNotif, Operator: d, Descriptor: &Task{}, FieldMask: "send_notif", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskNotifTypeNotIn struct {
	NotifType []NotifType
}

func (c TaskNotifTypeNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "notif_type", Value: c.NotifType, Operator: d, Descriptor: &Task{}, FieldMask: "notif_type", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskAssigneeNotIn struct {
	Assignee []string
}

func (c TaskAssigneeNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "assignee", Value: c.Assignee, Operator: d, Descriptor: &Task{}, FieldMask: "assignee", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskStatusNotIn struct {
	Status []Status
}

func (c TaskStatusNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "status", Value: c.Status, Operator: d, Descriptor: &Task{}, FieldMask: "status", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

type TaskHighPriorNotIn struct {
	HighPrior []bool
}

func (c TaskHighPriorNotIn) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "high_prior", Value: c.HighPrior, Operator: d, Descriptor: &Task{}, FieldMask: "high_prior", RootDescriptor: &Task{}, CurrentDescriptor: &Task{}}
}

func (c TrueCondition) taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner {
	return driver.TrueCondition{Operator: d}
}

type taskMapperObject struct {
	id           string
	title        string
	dueTimestamp *timestamp.Timestamp
	sendNotif    bool
	notifType    NotifType
	assignee     string
	status       Status
	highPrior    bool
}

func (s *taskMapperObject) GetUniqueIdentifier() string {
	return s.id
}

func MapperTask(rows []*Task) []*Task {

	ids := make([]string, 0, len(rows))
	uniqueIDMap := map[string]bool{}
	for _, r := range rows {
		if uniqueIDMap[r.Id] {
			continue
		}
		uniqueIDMap[r.Id] = true
		ids = append(ids, r.Id)
	}

	combinedTaskMappers := map[string]*taskMapperObject{}

	for _, rw := range rows {

		tempTask := &taskMapperObject{}

		if rw == nil {
			rw = rw.GetEmptyObject()
		}
		tempTask.id = rw.Id
		tempTask.title = rw.Title
		tempTask.dueTimestamp = rw.DueTimestamp
		tempTask.sendNotif = rw.SendNotif
		tempTask.notifType = rw.NotifType
		tempTask.assignee = rw.Assignee
		tempTask.status = rw.Status
		tempTask.highPrior = rw.HighPrior

		if combinedTaskMappers[tempTask.GetUniqueIdentifier()] == nil {
			combinedTaskMappers[tempTask.GetUniqueIdentifier()] = tempTask
		}
	}

	combinedTasks := make(map[string]*Task, 0)

	for _, task := range combinedTaskMappers {
		tempTask := &Task{}
		tempTask.Id = task.id
		tempTask.Title = task.title
		tempTask.DueTimestamp = task.dueTimestamp
		tempTask.SendNotif = task.sendNotif
		tempTask.NotifType = task.notifType
		tempTask.Assignee = task.assignee
		tempTask.Status = task.status
		tempTask.HighPrior = task.highPrior

		if tempTask.Id == "" {
			continue
		}

		combinedTasks[tempTask.Id] = tempTask

	}
	list := make([]*Task, 0, len(combinedTasks))
	for _, i := range ids {
		list = append(list, combinedTasks[i])
	}
	return list
}

func (m *Task) IsUsedMultipleTimes(f string) bool {
	return false
}

func (m *TaskActivity) PackageName() string {
	return "saastack_task_v1"
}

func (m *TaskActivity) TableOfObject(f, s string) string {
	return objectTableMap[f][s]
}

func (m *TaskActivity) GetDescriptorsOf(f string) (driver.Descriptor, error) {
	switch f {
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *TaskActivity) ObjectName() string {
	return "task_activity"
}

func (m *TaskActivity) Fields() []string {
	return []string{
		"id", "task_id", "msg",
	}
}

func (m *TaskActivity) IsObject(field string) bool {
	switch field {
	default:
		return false
	}
}

func (m *TaskActivity) ValuerSlice(field string) ([]driver.Descriptor, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	default:
		return []driver.Descriptor{}, errors.ErrInvalidField
	}
}

func (m *TaskActivity) Valuer(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return m.Id, nil
	case "task_id":
		return m.TaskId, nil
	case "msg":
		return m.Msg, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *TaskActivity) Addresser(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return &m.Id, nil
	case "task_id":
		return &m.TaskId, nil
	case "msg":
		return &m.Msg, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *TaskActivity) New(field string) error {
	switch field {
	case "id":
		return nil
	case "task_id":
		return nil
	case "msg":
		return nil
	default:
		return errors.ErrInvalidField
	}
}

func (m *TaskActivity) Type(field string) string {
	switch field {
	case "id":
		return "string"
	case "task_id":
		return "string"
	case "msg":
		return "string"
	default:
		return ""
	}
}

func (_ *TaskActivity) GetEmptyObject() (m *TaskActivity) {
	m = &TaskActivity{}
	return
}

func (m *TaskActivity) GetPrefix() string {
	return "act"
}

func (m *TaskActivity) GetID() string {
	return m.Id
}

func (m *TaskActivity) SetID(id string) {
	m.Id = id
}

func (m *TaskActivity) IsRoot() bool {
	return true
}

func (m *TaskActivity) IsFlatObject(f string) bool {
	return false
}

func (m *TaskActivity) NoOfParents(d driver.Descriptor) int {
	switch d.ObjectName() {
	}
	return 0
}

type TaskActivityStore struct {
	d      driver.Driver
	withTx bool
	tx     driver.Transaction

	limitMultiplier int
}

func (s TaskActivityStore) Execute(ctx context.Context, query string, args ...interface{}) error {
	if s.withTx {
		return s.tx.Execute(ctx, query, args...)
	}
	return s.d.Execute(ctx, query, args...)
}

func (s TaskActivityStore) QueryRows(ctx context.Context, query string, scanners []string, args ...interface{}) (driver.Result, error) {
	if s.withTx {
		return s.tx.QueryRows(ctx, query, scanners, args...)
	}
	return s.d.QueryRows(ctx, query, scanners, args...)
}

func NewTaskActivityStore(d driver.Driver) TaskActivityStore {
	return TaskActivityStore{d: d, limitMultiplier: 1}
}

func NewPostgresTaskActivityStore(db *x.DB, usr driver.IUserInfo) TaskActivityStore {
	return TaskActivityStore{
		d:               &sql.Sql{DB: db, UserInfo: usr, Placeholder: sqrl.Dollar},
		limitMultiplier: 1,
	}
}

type TaskActivityTx struct {
	TaskActivityStore
}

func (s TaskActivityStore) BeginTx(ctx context.Context) (*TaskActivityTx, error) {
	tx, err := s.d.BeginTx(ctx)
	if err != nil {
		return nil, err
	}
	return &TaskActivityTx{
		TaskActivityStore: TaskActivityStore{
			d:      s.d,
			withTx: true,
			tx:     tx,
		},
	}, nil
}

func (tx *TaskActivityTx) Commit(ctx context.Context) error {
	return tx.tx.Commit(ctx)
}

func (tx *TaskActivityTx) RollBack(ctx context.Context) error {
	return tx.tx.RollBack(ctx)
}

func (s TaskActivityStore) CreateTaskActivityPGStore(ctx context.Context) error {
	const queries = `
CREATE SCHEMA IF NOT EXISTS saastack_task_v1;
CREATE TABLE IF NOT EXISTS  saastack_task_v1.task_activity( id text DEFAULT ''::text , task_id text DEFAULT ''::text , msg text DEFAULT ''::text , parent text DEFAULT ''::text , is_deleted boolean DEFAULT false, deleted_by text DEFAULT ''::text, deleted_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, updated_by text DEFAULT ''::text, updated_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, created_by text DEFAULT ''::text, created_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, field_variable_mask text DEFAULT ''::text, PRIMARY KEY (id, parent)); 
CREATE TABLE IF NOT EXISTS  saastack_task_v1.task_activity_parent( id text DEFAULT ''::text , parent text DEFAULT ''::text ); 
`
	if err := s.d.Execute(ctx, queries); err != nil {
		return err
	}
	return nil
}

func (s TaskActivityStore) CreateTaskActivitys(ctx context.Context, list ...*TaskActivity) ([]string, error) {
	vv := make([]driver.Descriptor, len(list))
	for i := range list {
		vv[i] = list[i]
	}
	if s.withTx {
		return s.tx.Insert(ctx, vv, &TaskActivity{}, &TaskActivity{}, "", []string{})
	}
	return s.d.Insert(ctx, vv, &TaskActivity{}, &TaskActivity{}, "", []string{})
}

func (s TaskActivityStore) DeleteTaskActivity(ctx context.Context, cond TaskActivityCondition) error {
	if s.withTx {
		return s.tx.Delete(ctx, cond.taskActivityCondToDriverTaskActivityCond(s.d), &TaskActivity{}, &TaskActivity{})
	}
	return s.d.Delete(ctx, cond.taskActivityCondToDriverTaskActivityCond(s.d), &TaskActivity{}, &TaskActivity{})
}

func (s TaskActivityStore) UpdateTaskActivity(ctx context.Context, req *TaskActivity, fields []string, cond TaskActivityCondition) error {
	if s.withTx {
		return s.tx.Update(ctx, cond.taskActivityCondToDriverTaskActivityCond(s.d), req, &TaskActivity{}, fields...)
	}
	return s.d.Update(ctx, cond.taskActivityCondToDriverTaskActivityCond(s.d), req, &TaskActivity{}, fields...)
}

func (s TaskActivityStore) UpdateTaskActivityMetaInfo(ctx context.Context, list ...*driver.UpdateMetaInfoRequest) error {
	fn := s.d.UpdateMetaInfo
	if s.withTx {
		fn = s.tx.UpdateMetaInfo
	}
	return fn(ctx, &TaskActivity{}, &TaskActivity{}, list...)
}

func (s TaskActivityStore) GetTaskActivity(ctx context.Context, fields []string, cond TaskActivityCondition, opt ...getTaskActivitysOption) (*TaskActivity, error) {
	if len(fields) == 0 {
		fields = (&TaskActivity{}).Fields()
	}
	m := MetaInfoForList{}
	listOpts := []listTaskActivitysOption{
		&CursorBasedPagination{Limit: 1},
	}
	for _, o := range opt {
		t, _ := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			listOpts = append(listOpts, &m)
		}
	}
	objList, err := s.ListTaskActivitys(ctx, fields, cond, listOpts...)
	if len(objList) == 0 && err == nil {
		err = errors.ErrNotFound
	}
	if err != nil {
		return nil, err
	}
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			in.(*MetaInfo).UpdatedBy = m[0].UpdatedBy
			in.(*MetaInfo).CreatedBy = m[0].CreatedBy
			in.(*MetaInfo).DeletedBy = m[0].DeletedBy
			in.(*MetaInfo).UpdatedOn = m[0].UpdatedOn
			in.(*MetaInfo).CreatedOn = m[0].CreatedOn
			in.(*MetaInfo).DeletedOn = m[0].DeletedOn
			in.(*MetaInfo).IsDeleted = m[0].IsDeleted
		}
	}
	return objList[0], nil
}

func (s TaskActivityStore) ListTaskActivitys(ctx context.Context, fields []string, cond TaskActivityCondition, opt ...listTaskActivitysOption) ([]*TaskActivity, error) {
	if len(fields) == 0 {
		fields = (&TaskActivity{}).Fields()
	}
	var (
		res driver.Result
		err error
		m   driver.MetaInfo

		limit       = -1
		orderByList = make([]driver.OrderByType, 0, 5)
	)
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			page, ok := in.(*CursorBasedPagination)
			if page != nil && ok {
				if page.SetTaskActivityCondition == nil {
					page.SetTaskActivityCondition = defaultSetTaskActivityCondition
				}
				cond = page.SetTaskActivityCondition(page.UpOrDown, page.Cursor, cond)
				limit = page.Limit + 1
				if len(orderByList) == 0 {
					orderByList = append(orderByList, driver.OrderByType{
						Field:     "id",
						Ascending: !page.UpOrDown,
					})
				}
			}
		case driver.OptionType_MetaInfo:
			s.d.MetaInfoRequested(&ctx, &m)
		case driver.OptionType_OrderBy:
			by, ok := in.(OrderBy)
			if ok && len(by.Bys) != 0 {
				orderByList = by.Bys
			}
		}
	}
	if len(orderByList) == 0 {
		orderByList = append(orderByList, driver.OrderByType{
			Field:     "id",
			Ascending: true,
		})
	}
	ctx = driver.SetOrderBy(ctx, orderByList...)
	if limit > 0 {
		ctx = driver.SetListLimit(ctx, limit)
	}

	if s.withTx {
		res, err = s.tx.Get(ctx, cond.taskActivityCondToDriverTaskActivityCond(s.d), &TaskActivity{}, &TaskActivity{}, fields...)
	} else {
		res, err = s.d.Get(ctx, cond.taskActivityCondToDriverTaskActivityCond(s.d), &TaskActivity{}, &TaskActivity{}, fields...)
	}
	if err != nil {
		return nil, err
	}
	defer res.Close()

	mp := map[string]struct{}{}
	list := make([]*TaskActivity, 0, 1000)
	infoMap := make(map[string]*driver.MetaInfo, 0)

	for res.Next(ctx) && limit != 0 {
		obj := &TaskActivity{}
		if err := res.Scan(ctx, obj); err != nil {
			return nil, err
		}
		for _, o := range opt {
			t, _ := o.getValue()
			switch t {
			case driver.OptionType_MetaInfo:
				infoMap[obj.Id] = &driver.MetaInfo{
					UpdatedBy: m.UpdatedBy,
					CreatedBy: m.CreatedBy,
					DeletedBy: m.DeletedBy,
					UpdatedOn: m.UpdatedOn,
					CreatedOn: m.CreatedOn,
					DeletedOn: m.DeletedOn,
					IsDeleted: m.IsDeleted,
				}
				break
			}
		}
		list = append(list, obj)
		if _, ok := mp[obj.Id]; !ok {
			limit--
			mp[obj.Id] = struct{}{}
		}
	}
	if err := res.Close(); err != nil {
		return nil, err
	}

	list = MapperTaskActivity(list)
	meta := &MetaInfoForList{}

	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			page, ok := in.(*CursorBasedPagination)
			if page != nil && ok {
				if len(list) <= page.Limit {
					page.HasNext = false
					page.HasPrevious = false
				} else {
					list = list[:page.Limit]
					if page.UpOrDown {
						page.HasPrevious = true
					} else {
						page.HasNext = true
					}
				}
			}
		case driver.OptionType_MetaInfo:
			meta = in.(*MetaInfoForList)
		}
	}
	for _, l := range list {
		*meta = append(*meta, infoMap[l.Id])
	}
	return list, nil
}

func (s TaskActivityStore) CountTaskActivitys(ctx context.Context, cond TaskActivityCondition) (int, error) {
	cntFn := s.d.Count
	if s.withTx {
		cntFn = s.tx.Count
	}
	return cntFn(ctx, cond.taskActivityCondToDriverTaskActivityCond(s.d), &TaskActivity{}, &TaskActivity{})
}

type getTaskActivitysOption interface {
	getOptTaskActivitys() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfo) getOptTaskActivitys() { // method of no significant use
}

type listTaskActivitysOption interface {
	listOptTaskActivitys() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfoForList) listOptTaskActivitys() {
}

func (OrderBy) listOptTaskActivitys() {
}

func (*CursorBasedPagination) listOptTaskActivitys() {
}

func defaultSetTaskActivityCondition(upOrDown bool, cursor string, cond TaskActivityCondition) TaskActivityCondition {
	if upOrDown {
		if cursor != "" {
			return TaskActivityAnd{cond, TaskActivityIdLt{cursor}}
		}
		return cond
	}
	if cursor != "" {
		return TaskActivityAnd{cond, TaskActivityIdGt{cursor}}
	}
	return cond
}

type TaskActivityAnd []TaskActivityCondition

func (p TaskActivityAnd) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.taskActivityCondToDriverTaskActivityCond(d))
	}
	return driver.And{Conditioners: dc, Operator: d}
}

type TaskActivityOr []TaskActivityCondition

func (p TaskActivityOr) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.taskActivityCondToDriverTaskActivityCond(d))
	}
	return driver.Or{Conditioners: dc, Operator: d}
}

type TaskActivityParentEq struct {
	Parent string
}

func (c TaskActivityParentEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityFullParentEq struct {
	Parent string
}

func (c TaskActivityFullParentEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityParentNotEq struct {
	Parent string
}

func (c TaskActivityParentNotEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityFullParentNotEq struct {
	Parent string
}

func (c TaskActivityFullParentNotEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityParentLike struct {
	Parent string
}

func (c TaskActivityParentLike) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityFullParentLike struct {
	Parent string
}

func (c TaskActivityFullParentLike) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityParentILike struct {
	Parent string
}

func (c TaskActivityParentILike) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityFullParentILike struct {
	Parent string
}

func (c TaskActivityFullParentILike) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityParentIn struct {
	Parent []string
}

func (c TaskActivityParentIn) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityFullParentIn struct {
	Parent []string
}

func (c TaskActivityFullParentIn) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityParentNotIn struct {
	Parent []string
}

func (c TaskActivityParentNotIn) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityFullParentNotIn struct {
	Parent []string
}

func (c TaskActivityFullParentNotIn) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityIdEq struct {
	Id string
}

func (c TaskActivityIdEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "id", Value: c.Id, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "id", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityTaskIdEq struct {
	TaskId string
}

func (c TaskActivityTaskIdEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "task_id", Value: c.TaskId, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "task_id", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityMsgEq struct {
	Msg string
}

func (c TaskActivityMsgEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "msg", Value: c.Msg, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "msg", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityIdNotEq struct {
	Id string
}

func (c TaskActivityIdNotEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "id", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityTaskIdNotEq struct {
	TaskId string
}

func (c TaskActivityTaskIdNotEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "task_id", Value: c.TaskId, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "task_id", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityMsgNotEq struct {
	Msg string
}

func (c TaskActivityMsgNotEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "msg", Value: c.Msg, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "msg", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityIdGt struct {
	Id string
}

func (c TaskActivityIdGt) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "id", Value: c.Id, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "id", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityTaskIdGt struct {
	TaskId string
}

func (c TaskActivityTaskIdGt) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "task_id", Value: c.TaskId, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "task_id", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityMsgGt struct {
	Msg string
}

func (c TaskActivityMsgGt) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "msg", Value: c.Msg, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "msg", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityIdLt struct {
	Id string
}

func (c TaskActivityIdLt) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "id", Value: c.Id, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "id", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityTaskIdLt struct {
	TaskId string
}

func (c TaskActivityTaskIdLt) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "task_id", Value: c.TaskId, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "task_id", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityMsgLt struct {
	Msg string
}

func (c TaskActivityMsgLt) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "msg", Value: c.Msg, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "msg", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityIdGtOrEq struct {
	Id string
}

func (c TaskActivityIdGtOrEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "id", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityTaskIdGtOrEq struct {
	TaskId string
}

func (c TaskActivityTaskIdGtOrEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "task_id", Value: c.TaskId, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "task_id", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityMsgGtOrEq struct {
	Msg string
}

func (c TaskActivityMsgGtOrEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "msg", Value: c.Msg, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "msg", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityIdLtOrEq struct {
	Id string
}

func (c TaskActivityIdLtOrEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "id", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityTaskIdLtOrEq struct {
	TaskId string
}

func (c TaskActivityTaskIdLtOrEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "task_id", Value: c.TaskId, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "task_id", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityMsgLtOrEq struct {
	Msg string
}

func (c TaskActivityMsgLtOrEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "msg", Value: c.Msg, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "msg", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityIdLike struct {
	Id string
}

func (c TaskActivityIdLike) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "id", Value: c.Id, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "id", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityTaskIdLike struct {
	TaskId string
}

func (c TaskActivityTaskIdLike) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "task_id", Value: c.TaskId, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "task_id", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityMsgLike struct {
	Msg string
}

func (c TaskActivityMsgLike) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "msg", Value: c.Msg, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "msg", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityIdILike struct {
	Id string
}

func (c TaskActivityIdILike) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "id", Value: c.Id, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "id", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityTaskIdILike struct {
	TaskId string
}

func (c TaskActivityTaskIdILike) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "task_id", Value: c.TaskId, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "task_id", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityMsgILike struct {
	Msg string
}

func (c TaskActivityMsgILike) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "msg", Value: c.Msg, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "msg", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityDeleted struct {
	IsDeleted bool
}

func (c TaskActivityDeleted) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: c.IsDeleted, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "id", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityCreatedByEq struct {
	By string
}

func (c TaskActivityCreatedByEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityCreatedOnEq struct {
	On *timestamp.Timestamp
}

func (c TaskActivityCreatedOnEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityCreatedByNotEq struct {
	By string
}

func (c TaskActivityCreatedByNotEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityCreatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c TaskActivityCreatedOnNotEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityCreatedByGt struct {
	By string
}

func (c TaskActivityCreatedByGt) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityCreatedOnGt struct {
	On *timestamp.Timestamp
}

func (c TaskActivityCreatedOnGt) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityCreatedByLt struct {
	By string
}

func (c TaskActivityCreatedByLt) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityCreatedOnLt struct {
	On *timestamp.Timestamp
}

func (c TaskActivityCreatedOnLt) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityCreatedByGtOrEq struct {
	By string
}

func (c TaskActivityCreatedByGtOrEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityCreatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskActivityCreatedOnGtOrEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityCreatedByLtOrEq struct {
	By string
}

func (c TaskActivityCreatedByLtOrEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityCreatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskActivityCreatedOnLtOrEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityCreatedByLike struct {
	By string
}

func (c TaskActivityCreatedByLike) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "created_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityCreatedByILike struct {
	By string
}

func (c TaskActivityCreatedByILike) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "created_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityUpdatedByEq struct {
	By string
}

func (c TaskActivityUpdatedByEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityUpdatedOnEq struct {
	On *timestamp.Timestamp
}

func (c TaskActivityUpdatedOnEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityUpdatedByNotEq struct {
	By string
}

func (c TaskActivityUpdatedByNotEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityUpdatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c TaskActivityUpdatedOnNotEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityUpdatedByGt struct {
	By string
}

func (c TaskActivityUpdatedByGt) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityUpdatedOnGt struct {
	On *timestamp.Timestamp
}

func (c TaskActivityUpdatedOnGt) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityUpdatedByLt struct {
	By string
}

func (c TaskActivityUpdatedByLt) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityUpdatedOnLt struct {
	On *timestamp.Timestamp
}

func (c TaskActivityUpdatedOnLt) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityUpdatedByGtOrEq struct {
	By string
}

func (c TaskActivityUpdatedByGtOrEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityUpdatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskActivityUpdatedOnGtOrEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityUpdatedByLtOrEq struct {
	By string
}

func (c TaskActivityUpdatedByLtOrEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityUpdatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskActivityUpdatedOnLtOrEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityUpdatedByLike struct {
	By string
}

func (c TaskActivityUpdatedByLike) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityUpdatedByILike struct {
	By string
}

func (c TaskActivityUpdatedByILike) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityDeletedByEq struct {
	By string
}

func (c TaskActivityDeletedByEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityDeletedOnEq struct {
	On *timestamp.Timestamp
}

func (c TaskActivityDeletedOnEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityDeletedByNotEq struct {
	By string
}

func (c TaskActivityDeletedByNotEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityDeletedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c TaskActivityDeletedOnNotEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityDeletedByGt struct {
	By string
}

func (c TaskActivityDeletedByGt) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityDeletedOnGt struct {
	On *timestamp.Timestamp
}

func (c TaskActivityDeletedOnGt) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityDeletedByLt struct {
	By string
}

func (c TaskActivityDeletedByLt) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityDeletedOnLt struct {
	On *timestamp.Timestamp
}

func (c TaskActivityDeletedOnLt) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityDeletedByGtOrEq struct {
	By string
}

func (c TaskActivityDeletedByGtOrEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityDeletedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskActivityDeletedOnGtOrEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityDeletedByLtOrEq struct {
	By string
}

func (c TaskActivityDeletedByLtOrEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityDeletedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c TaskActivityDeletedOnLtOrEq) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityDeletedByLike struct {
	By string
}

func (c TaskActivityDeletedByLike) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityDeletedByILike struct {
	By string
}

func (c TaskActivityDeletedByILike) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &TaskActivity{}, RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityIdIn struct {
	Id []string
}

func (c TaskActivityIdIn) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "id", Value: c.Id, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "id", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityTaskIdIn struct {
	TaskId []string
}

func (c TaskActivityTaskIdIn) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "task_id", Value: c.TaskId, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "task_id", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityMsgIn struct {
	Msg []string
}

func (c TaskActivityMsgIn) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "msg", Value: c.Msg, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "msg", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityIdNotIn struct {
	Id []string
}

func (c TaskActivityIdNotIn) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "id", Value: c.Id, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "id", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityTaskIdNotIn struct {
	TaskId []string
}

func (c TaskActivityTaskIdNotIn) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "task_id", Value: c.TaskId, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "task_id", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

type TaskActivityMsgNotIn struct {
	Msg []string
}

func (c TaskActivityMsgNotIn) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "msg", Value: c.Msg, Operator: d, Descriptor: &TaskActivity{}, FieldMask: "msg", RootDescriptor: &TaskActivity{}, CurrentDescriptor: &TaskActivity{}}
}

func (c TrueCondition) taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner {
	return driver.TrueCondition{Operator: d}
}

type taskActivityMapperObject struct {
	id     string
	taskId string
	msg    string
}

func (s *taskActivityMapperObject) GetUniqueIdentifier() string {
	return s.id
}

func MapperTaskActivity(rows []*TaskActivity) []*TaskActivity {

	ids := make([]string, 0, len(rows))
	uniqueIDMap := map[string]bool{}
	for _, r := range rows {
		if uniqueIDMap[r.Id] {
			continue
		}
		uniqueIDMap[r.Id] = true
		ids = append(ids, r.Id)
	}

	combinedTaskActivityMappers := map[string]*taskActivityMapperObject{}

	for _, rw := range rows {

		tempTaskActivity := &taskActivityMapperObject{}

		if rw == nil {
			rw = rw.GetEmptyObject()
		}
		tempTaskActivity.id = rw.Id
		tempTaskActivity.taskId = rw.TaskId
		tempTaskActivity.msg = rw.Msg

		if combinedTaskActivityMappers[tempTaskActivity.GetUniqueIdentifier()] == nil {
			combinedTaskActivityMappers[tempTaskActivity.GetUniqueIdentifier()] = tempTaskActivity
		}
	}

	combinedTaskActivitys := make(map[string]*TaskActivity, 0)

	for _, taskActivity := range combinedTaskActivityMappers {
		tempTaskActivity := &TaskActivity{}
		tempTaskActivity.Id = taskActivity.id
		tempTaskActivity.TaskId = taskActivity.taskId
		tempTaskActivity.Msg = taskActivity.msg

		if tempTaskActivity.Id == "" {
			continue
		}

		combinedTaskActivitys[tempTaskActivity.Id] = tempTaskActivity

	}
	list := make([]*TaskActivity, 0, len(combinedTaskActivitys))
	for _, i := range ids {
		list = append(list, combinedTaskActivitys[i])
	}
	return list
}

func (m *TaskActivity) IsUsedMultipleTimes(f string) bool {
	return false
}

type TrueCondition struct{}

type TaskCondition interface {
	taskCondToDriverTaskCond(d driver.Driver) driver.Conditioner
}
type TaskActivityCondition interface {
	taskActivityCondToDriverTaskActivityCond(d driver.Driver) driver.Conditioner
}

type CursorBasedPagination struct {
	// Set UpOrDown = true for getting list of data above Cursor-ID,
	// limited to 'limit' amount, when ordered by ID in Ascending order.
	// Set UpOrDown = false for getting list of data below Cursor-ID,
	// limited to 'limit' amount, when ordered by ID in Ascending order.
	Cursor   string
	Limit    int
	UpOrDown bool

	// All pagination-cursor condition functions for different objects
	// SetTaskCondition will be used to set the condition parameter for
	// setting parameter based on UpOrDown value,
	// if null default IdGt or IdLt condition will be used.
	SetTaskCondition func(upOrDown bool, cursor string, cond TaskCondition) TaskCondition
	// SetTaskActivityCondition will be used to set the condition parameter for
	// setting parameter based on UpOrDown value,
	// if null default IdGt or IdLt condition will be used.
	SetTaskActivityCondition func(upOrDown bool, cursor string, cond TaskActivityCondition) TaskActivityCondition

	// Response objects Items - will be updated and set after the list call
	HasNext     bool // Used in case of UpOrDown = false
	HasPrevious bool // Used in case of UpOrDown = true
}

func (p *CursorBasedPagination) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_Pagination, p
}

type MetaInfo driver.MetaInfo
type MetaInfoForList []*driver.MetaInfo

func (p *MetaInfo) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_MetaInfo, p
}

func (p *MetaInfoForList) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_MetaInfo, p
}

type OrderBy struct {
	Bys []driver.OrderByType
}

func (o OrderBy) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_OrderBy, o
}
