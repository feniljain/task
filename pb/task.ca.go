package pb

import (
	"context"

	. "github.com/golang/protobuf/ptypes/empty"
	"go.saastack.io/protoc-gen-caw/convert"
	"go.uber.org/cadence/activity"
	"go.uber.org/cadence/workflow"
)

var (
	_ = Empty{}
	_ = convert.JsonB{}
)

const (
	TasksCreateTaskActivity             = "/saastack.task.v1.Tasks/CreateTask"
	TasksGetTaskActivity                = "/saastack.task.v1.Tasks/GetTask"
	TasksDeleteTaskActivity             = "/saastack.task.v1.Tasks/DeleteTask"
	TasksUpdateTaskActivity             = "/saastack.task.v1.Tasks/UpdateTask"
	TasksListTaskActivity               = "/saastack.task.v1.Tasks/ListTask"
	TasksListTaskByTitleActivity        = "/saastack.task.v1.Tasks/ListTaskByTitle"
	TasksListTaskByStatusActivity       = "/saastack.task.v1.Tasks/ListTaskByStatus"
	TasksListTaskByTimestampActivity    = "/saastack.task.v1.Tasks/ListTaskByTimestamp"
	TasksBatchGetTaskActivity           = "/saastack.task.v1.Tasks/BatchGetTask"
	TasksGetTaskActivitiesActivity      = "/saastack.task.v1.Tasks/GetTaskActivities"
	TasksCreateTaskActivityActivity     = "/saastack.task.v1.Tasks/CreateTaskActivity"
	TasksGetTaskActivityActivity        = "/saastack.task.v1.Tasks/GetTaskActivity"
	TasksUpdateAssignedEmployeeActivity = "/saastack.task.v1.Tasks/UpdateAssignedEmployee"
	TasksGetTasksReportActivity         = "/saastack.task.v1.Tasks/GetTasksReport"
	TasksListTaskByProjectActivity      = "/saastack.task.v1.Tasks/ListTaskByProject"
)

func RegisterTasksActivities(cli TasksClient) {
	activity.RegisterWithOptions(
		func(ctx context.Context, in *CreateTaskRequest) (*Task, error) {
			res, err := cli.CreateTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksCreateTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *GetTaskRequest) (*Task, error) {
			res, err := cli.GetTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksGetTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *DeleteTaskRequest) (*Empty, error) {
			res, err := cli.DeleteTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksDeleteTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *UpdateTaskRequest) (*Task, error) {
			res, err := cli.UpdateTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksUpdateTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ListTaskRequest) (*ListTaskResponse, error) {
			res, err := cli.ListTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksListTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ListTaskByTitleRequest) (*ListTaskByTitleResponse, error) {
			res, err := cli.ListTaskByTitle(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksListTaskByTitleActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ListTaskByStatusRequest) (*ListTaskByStatusResponse, error) {
			res, err := cli.ListTaskByStatus(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksListTaskByStatusActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ListTaskByTimestampRequest) (*ListTaskByTimestampResponse, error) {
			res, err := cli.ListTaskByTimestamp(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksListTaskByTimestampActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *BatchGetTaskRequest) (*BatchGetTaskResponse, error) {
			res, err := cli.BatchGetTask(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksBatchGetTaskActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *GetTaskActivitiesRequest) (*GetTaskActivitiesResponse, error) {
			res, err := cli.GetTaskActivities(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksGetTaskActivitiesActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *CreateTaskActivityRequest) (*TaskActivity, error) {
			res, err := cli.CreateTaskActivity(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksCreateTaskActivityActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *GetTaskActivityRequest) (*TaskActivity, error) {
			res, err := cli.GetTaskActivity(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksGetTaskActivityActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *UpdateAssignedEmployeeRequest) (*Task, error) {
			res, err := cli.UpdateAssignedEmployee(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksUpdateAssignedEmployeeActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *GetTasksReportRequest) (*GetTasksReportResponse, error) {
			res, err := cli.GetTasksReport(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksGetTasksReportActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ListTaskByProjectRequest) (*ListTaskByProjectResponse, error) {
			res, err := cli.ListTaskByProject(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TasksListTaskByProjectActivity},
	)
}

// TasksActivitiesClient is a typesafe wrapper for TasksActivities.
type TasksActivitiesClient struct {
}

// NewTasksActivitiesClient creates a new TasksActivitiesClient.
func NewTasksActivitiesClient(cli TasksClient) TasksActivitiesClient {
	RegisterTasksActivities(cli)
	return TasksActivitiesClient{}
}

func (ca *TasksActivitiesClient) CreateTask(ctx workflow.Context, in *CreateTaskRequest) (*Task, error) {
	future := workflow.ExecuteActivity(ctx, TasksCreateTaskActivity, in)
	var result Task
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) GetTask(ctx workflow.Context, in *GetTaskRequest) (*Task, error) {
	future := workflow.ExecuteActivity(ctx, TasksGetTaskActivity, in)
	var result Task
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) DeleteTask(ctx workflow.Context, in *DeleteTaskRequest) (*Empty, error) {
	future := workflow.ExecuteActivity(ctx, TasksDeleteTaskActivity, in)
	var result Empty
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) UpdateTask(ctx workflow.Context, in *UpdateTaskRequest) (*Task, error) {
	future := workflow.ExecuteActivity(ctx, TasksUpdateTaskActivity, in)
	var result Task
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) ListTask(ctx workflow.Context, in *ListTaskRequest) (*ListTaskResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksListTaskActivity, in)
	var result ListTaskResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) ListTaskByTitle(ctx workflow.Context, in *ListTaskByTitleRequest) (*ListTaskByTitleResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksListTaskByTitleActivity, in)
	var result ListTaskByTitleResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) ListTaskByStatus(ctx workflow.Context, in *ListTaskByStatusRequest) (*ListTaskByStatusResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksListTaskByStatusActivity, in)
	var result ListTaskByStatusResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) ListTaskByTimestamp(ctx workflow.Context, in *ListTaskByTimestampRequest) (*ListTaskByTimestampResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksListTaskByTimestampActivity, in)
	var result ListTaskByTimestampResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) BatchGetTask(ctx workflow.Context, in *BatchGetTaskRequest) (*BatchGetTaskResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksBatchGetTaskActivity, in)
	var result BatchGetTaskResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) GetTaskActivities(ctx workflow.Context, in *GetTaskActivitiesRequest) (*GetTaskActivitiesResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksGetTaskActivitiesActivity, in)
	var result GetTaskActivitiesResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) CreateTaskActivity(ctx workflow.Context, in *CreateTaskActivityRequest) (*TaskActivity, error) {
	future := workflow.ExecuteActivity(ctx, TasksCreateTaskActivityActivity, in)
	var result TaskActivity
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) GetTaskActivity(ctx workflow.Context, in *GetTaskActivityRequest) (*TaskActivity, error) {
	future := workflow.ExecuteActivity(ctx, TasksGetTaskActivityActivity, in)
	var result TaskActivity
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) UpdateAssignedEmployee(ctx workflow.Context, in *UpdateAssignedEmployeeRequest) (*Task, error) {
	future := workflow.ExecuteActivity(ctx, TasksUpdateAssignedEmployeeActivity, in)
	var result Task
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) GetTasksReport(ctx workflow.Context, in *GetTasksReportRequest) (*GetTasksReportResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksGetTasksReportActivity, in)
	var result GetTasksReportResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TasksActivitiesClient) ListTaskByProject(ctx workflow.Context, in *ListTaskByProjectRequest) (*ListTaskByProjectResponse, error) {
	future := workflow.ExecuteActivity(ctx, TasksListTaskByProjectActivity, in)
	var result ListTaskByProjectResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

const (
	ParentServiceValidateParentActivity   = "/saastack.task.v1.ParentService/ValidateParent"
	ParentServiceValidateEmployeeActivity = "/saastack.task.v1.ParentService/ValidateEmployee"
)

func RegisterParentServiceActivities(cli ParentServiceClient) {
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ValidateParentRequest) (*ValidateParentResponse, error) {
			res, err := cli.ValidateParent(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: ParentServiceValidateParentActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ValidateEmployeeRequest) (*ValidateEmployeeResponse, error) {
			res, err := cli.ValidateEmployee(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: ParentServiceValidateEmployeeActivity},
	)
}

// ParentServiceActivitiesClient is a typesafe wrapper for ParentServiceActivities.
type ParentServiceActivitiesClient struct {
}

// NewParentServiceActivitiesClient creates a new ParentServiceActivitiesClient.
func NewParentServiceActivitiesClient(cli ParentServiceClient) ParentServiceActivitiesClient {
	RegisterParentServiceActivities(cli)
	return ParentServiceActivitiesClient{}
}

func (ca *ParentServiceActivitiesClient) ValidateParent(ctx workflow.Context, in *ValidateParentRequest) (*ValidateParentResponse, error) {
	future := workflow.ExecuteActivity(ctx, ParentServiceValidateParentActivity, in)
	var result ValidateParentResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *ParentServiceActivitiesClient) ValidateEmployee(ctx workflow.Context, in *ValidateEmployeeRequest) (*ValidateEmployeeResponse, error) {
	future := workflow.ExecuteActivity(ctx, ParentServiceValidateEmployeeActivity, in)
	var result ValidateEmployeeResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}
