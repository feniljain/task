// Code generated by protoc-gen-go. DO NOT EDIT.
// source: task.proto
package pb

import (
	"go.saastack.io/protoc-gen-grpc-wrapper/middleware"
	"go.uber.org/fx"
)

// UnregisteredMethods_Tasks will provide list of methods to un register from GRPC server
var UnregisteredMethods_Tasks = fx.Provide(
	fx.Annotated{
		Group:  "methods",
		Target: _Tasks_methodsList,
	},
)

func _Tasks_methodsList() middleware.UnregisteredMethods {
	return middleware.UnregisteredMethods{
		"/saastack.task.v1.Tasks/BatchGetTask",
	}
}

// UnregisteredMethods_ParentService will provide list of methods to un register from GRPC server
var UnregisteredMethods_ParentService = fx.Provide(
	fx.Annotated{
		Group:  "methods",
		Target: _ParentService_methodsList,
	},
)

func _ParentService_methodsList() middleware.UnregisteredMethods {
	return middleware.UnregisteredMethods{
		"/saastack.task.v1.ParentService/ValidateParent",
		"/saastack.task.v1.ParentService/ValidateEmployee",
	}
}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *Task) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetDueTimestamp()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcResonse()
	}

	if face, ok := interface{}(m.GetCreatedOn()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcResonse()
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *Task) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetDueTimestamp()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcRequest()
	}

	if face, ok := interface{}(m.GetCreatedOn()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcRequest()
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *TaskActivity) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetCreatedOn()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcResonse()
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *TaskActivity) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetCreatedOn()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcRequest()
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *GetTaskActivityRequest) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *GetTaskActivityRequest) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *GetTaskActivitiesRequest) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *GetTaskActivitiesRequest) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *GetTaskActivitiesResponse) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

	for _, item := range m.GetTaskActivity() {
		if face, ok := interface{}(item).(middleware.GrpcRequestResponseMasker); ok {
			face.MaskFieldsFromGrpcResonse()
		}
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *GetTaskActivitiesResponse) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

	for _, item := range m.GetTaskActivity() {
		if face, ok := interface{}(item).(middleware.GrpcRequestResponseMasker); ok {
			face.MaskFieldsFromGrpcRequest()
		}
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *CreateTaskRequest) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetTask()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcResonse()
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *CreateTaskRequest) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetTask()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcRequest()
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *CreateTaskActivityRequest) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetTaskActivity()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcResonse()
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *CreateTaskActivityRequest) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetTaskActivity()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcRequest()
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *GetTaskRequest) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetViewMask()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcResonse()
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *GetTaskRequest) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetViewMask()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcRequest()
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *DeleteTaskRequest) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *DeleteTaskRequest) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *UpdateTaskRequest) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetTask()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcResonse()
	}

	if face, ok := interface{}(m.GetUpdateMask()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcResonse()
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *UpdateTaskRequest) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetTask()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcRequest()
	}

	if face, ok := interface{}(m.GetUpdateMask()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcRequest()
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *ListTaskRequest) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetViewMask()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcResonse()
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *ListTaskRequest) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetViewMask()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcRequest()
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *ListTaskResponse) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

	for _, item := range m.GetTask() {
		if face, ok := interface{}(item).(middleware.GrpcRequestResponseMasker); ok {
			face.MaskFieldsFromGrpcResonse()
		}
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *ListTaskResponse) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

	for _, item := range m.GetTask() {
		if face, ok := interface{}(item).(middleware.GrpcRequestResponseMasker); ok {
			face.MaskFieldsFromGrpcRequest()
		}
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *ListTaskByTitleRequest) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetViewMask()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcResonse()
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *ListTaskByTitleRequest) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetViewMask()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcRequest()
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *ListTaskByTitleResponse) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

	for _, item := range m.GetTask() {
		if face, ok := interface{}(item).(middleware.GrpcRequestResponseMasker); ok {
			face.MaskFieldsFromGrpcResonse()
		}
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *ListTaskByTitleResponse) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

	for _, item := range m.GetTask() {
		if face, ok := interface{}(item).(middleware.GrpcRequestResponseMasker); ok {
			face.MaskFieldsFromGrpcRequest()
		}
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *ListTaskByStatusRequest) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetViewMask()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcResonse()
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *ListTaskByStatusRequest) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetViewMask()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcRequest()
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *ListTaskByStatusResponse) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

	for _, item := range m.GetTask() {
		if face, ok := interface{}(item).(middleware.GrpcRequestResponseMasker); ok {
			face.MaskFieldsFromGrpcResonse()
		}
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *ListTaskByStatusResponse) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

	for _, item := range m.GetTask() {
		if face, ok := interface{}(item).(middleware.GrpcRequestResponseMasker); ok {
			face.MaskFieldsFromGrpcRequest()
		}
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *ListTaskByTimestampRequest) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetTimestamp()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcResonse()
	}

	if face, ok := interface{}(m.GetViewMask()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcResonse()
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *ListTaskByTimestampRequest) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetTimestamp()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcRequest()
	}

	if face, ok := interface{}(m.GetViewMask()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcRequest()
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *ListTaskByTimestampResponse) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

	for _, item := range m.GetTask() {
		if face, ok := interface{}(item).(middleware.GrpcRequestResponseMasker); ok {
			face.MaskFieldsFromGrpcResonse()
		}
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *ListTaskByTimestampResponse) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

	for _, item := range m.GetTask() {
		if face, ok := interface{}(item).(middleware.GrpcRequestResponseMasker); ok {
			face.MaskFieldsFromGrpcRequest()
		}
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *BatchGetTaskRequest) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetViewMask()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcResonse()
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *BatchGetTaskRequest) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetViewMask()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcRequest()
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *BatchGetTaskResponse) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

	for _, item := range m.GetTask() {
		if face, ok := interface{}(item).(middleware.GrpcRequestResponseMasker); ok {
			face.MaskFieldsFromGrpcResonse()
		}
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *BatchGetTaskResponse) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

	for _, item := range m.GetTask() {
		if face, ok := interface{}(item).(middleware.GrpcRequestResponseMasker); ok {
			face.MaskFieldsFromGrpcRequest()
		}
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *UpdateAssignedEmployeeRequest) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *UpdateAssignedEmployeeRequest) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *GetTasksReportRequest) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetStartTimestamp()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcResonse()
	}

	if face, ok := interface{}(m.GetEndTimestamp()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcResonse()
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *GetTasksReportRequest) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetStartTimestamp()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcRequest()
	}

	if face, ok := interface{}(m.GetEndTimestamp()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcRequest()
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *GetTasksReportResponse) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

	for _, item := range m.GetReports() {
		if face, ok := interface{}(item).(middleware.GrpcRequestResponseMasker); ok {
			face.MaskFieldsFromGrpcResonse()
		}
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *GetTasksReportResponse) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

	for _, item := range m.GetReports() {
		if face, ok := interface{}(item).(middleware.GrpcRequestResponseMasker); ok {
			face.MaskFieldsFromGrpcRequest()
		}
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *Report) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetTimestamp()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcResonse()
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *Report) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

	if face, ok := interface{}(m.GetTimestamp()).(middleware.GrpcRequestResponseMasker); ok {
		face.MaskFieldsFromGrpcRequest()
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *ListTaskByProjectRequest) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *ListTaskByProjectRequest) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *ListTaskByProjectResponse) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

	for _, item := range m.GetTasks() {
		if face, ok := interface{}(item).(middleware.GrpcRequestResponseMasker); ok {
			face.MaskFieldsFromGrpcResonse()
		}
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *ListTaskByProjectResponse) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

	for _, item := range m.GetTasks() {
		if face, ok := interface{}(item).(middleware.GrpcRequestResponseMasker); ok {
			face.MaskFieldsFromGrpcRequest()
		}
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *ValidateParentRequest) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *ValidateParentRequest) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *ValidateParentResponse) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *ValidateParentResponse) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *ValidateEmployeeRequest) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *ValidateEmployeeRequest) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

}

// MaskFieldsFromGrpcResonse will mask null the marked fields from response
func (m *ValidateEmployeeResponse) MaskFieldsFromGrpcResonse() {
	if m == nil {
		return
	}

}

// MaskFieldsFromGrpcRequest will mask null the marked fields from request
func (m *ValidateEmployeeResponse) MaskFieldsFromGrpcRequest() {
	if m == nil {
		return
	}

}
