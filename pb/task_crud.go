package pb

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"go.saastack.io/chaku/errors"
	"go.saastack.io/idutil"
	"go.saastack.io/userinfo"
	"google.golang.org/genproto/protobuf/field_mask"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type TasksServiceTaskActivityServerCrud struct {
	store TaskActivityStore
	bloc  TasksServiceTaskActivityServerBLoC
}

type TasksServiceTaskActivityServerBLoC interface {
	CreateTaskActivityBLoC(context.Context, *CreateTaskActivityRequest) error

	GetTaskActivityBLoC(context.Context, *GetTaskActivityRequest) error
}

func NewTasksServiceTaskActivityServerCrud(s TaskActivityStore, b TasksServiceTaskActivityServerBLoC) *TasksServiceTaskActivityServerCrud {
	return &TasksServiceTaskActivityServerCrud{store: s, bloc: b}
}

func (s *TasksServiceTaskActivityServerCrud) CreateTaskActivity(ctx context.Context, in *CreateTaskActivityRequest) (*TaskActivity, error) {

	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.CreateTaskActivityBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	if idutil.GetPrefix(in.TaskActivity.Id) != in.TaskActivity.GetPrefix() {
		in.TaskActivity.Id = in.Parent
	}

	ids, err := s.store.CreateTaskActivitys(ctx, in.TaskActivity)
	if err != nil {
		return nil, err
	}

	in.TaskActivity.Id = ids[0]

	return in.GetTaskActivity(), nil
}

func (s *TasksServiceTaskActivityServerCrud) GetTaskActivity(ctx context.Context, in *GetTaskActivityRequest) (*TaskActivity, error) {
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.GetTaskActivityBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	mask := (&TaskActivity{}).Fields()

	res, err := s.store.GetTaskActivity(ctx, mask, TaskActivityIdEq{Id: in.Id})
	if err != nil {
		if err == errors.ErrNotFound {
			return nil, status.Error(codes.NotFound, "TaskActivity not found")
		}
		return nil, err
	}

	return res, nil
}

func (s *TasksServiceTaskActivityServerCrud) GetViewMask(mask *field_mask.FieldMask) []string {
	if mask == nil || mask.GetPaths() == nil {
		return []string{}
	}
	return mask.GetPaths()
}

type TasksServiceTaskServerCrud struct {
	store TaskStore
	bloc  TasksServiceTaskServerBLoC
}

type TasksServiceTaskServerBLoC interface {
	CreateTaskBLoC(context.Context, *CreateTaskRequest) error

	GetTaskBLoC(context.Context, *GetTaskRequest) error

	UpdateTaskBLoC(context.Context, *UpdateTaskRequest) error

	DeleteTaskBLoC(context.Context, *DeleteTaskRequest) error

	BatchGetTaskBLoC(context.Context, *BatchGetTaskRequest) error

	ListTaskBLoC(context.Context, *ListTaskRequest) (TaskCondition, error)
}

func NewTasksServiceTaskServerCrud(s TaskStore, b TasksServiceTaskServerBLoC) *TasksServiceTaskServerCrud {
	return &TasksServiceTaskServerCrud{store: s, bloc: b}
}

func (s *TasksServiceTaskServerCrud) CreateTask(ctx context.Context, in *CreateTaskRequest) (*Task, error) {

	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.CreateTaskBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	if idutil.GetPrefix(in.Task.Id) != in.Task.GetPrefix() {
		in.Task.Id = in.Parent
	}

	ids, err := s.store.CreateTasks(ctx, in.Task)
	if err != nil {
		return nil, err
	}

	in.Task.Id = ids[0]

	return in.GetTask(), nil
}

func (s *TasksServiceTaskServerCrud) UpdateTask(ctx context.Context, in *UpdateTaskRequest) (*Task, error) {

	mask := s.GetViewMask(in.UpdateMask)
	if len(mask) == 0 {
		return nil, status.Error(codes.InvalidArgument, "cannot send empty update mask")
	}

	if err := in.GetTask().Validate(mask...); err != nil {
		return nil, err
	}

	err := s.bloc.UpdateTaskBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	if err := s.store.UpdateTask(ctx,
		in.Task, mask,
		TaskIdEq{Id: in.Task.Id},
	); err != nil {
		return nil, err
	}

	updatedTask, err := s.store.GetTask(ctx, []string{},
		TaskIdEq{
			Id: in.GetTask().GetId(),
		},
	)
	if err != nil {
		return nil, err
	}

	return updatedTask, nil
}

func (s *TasksServiceTaskServerCrud) GetTask(ctx context.Context, in *GetTaskRequest) (*Task, error) {
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.GetTaskBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	mask := s.GetViewMask(in.ViewMask)

	res, err := s.store.GetTask(ctx, mask, TaskIdEq{Id: in.Id})
	if err != nil {
		if err == errors.ErrNotFound {
			return nil, status.Error(codes.NotFound, "Task not found")
		}
		return nil, err
	}

	return res, nil
}

func (s *TasksServiceTaskServerCrud) ListTask(ctx context.Context, in *ListTaskRequest) (*ListTaskResponse, error) {

	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	condition, err := s.bloc.ListTaskBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	mask := s.GetViewMask(in.ViewMask)

	return s.ListWithoutPagination(ctx, condition, mask)
}

func (s *TasksServiceTaskServerCrud) ListWithoutPagination(ctx context.Context, condition TaskCondition, viewMask []string) (*ListTaskResponse, error) {

	list, err := s.store.ListTasks(ctx,
		viewMask,
		condition,
	)
	if err != nil {
		return nil, err
	}

	return &ListTaskResponse{Task: list}, err
}

func (s *TasksServiceTaskServerCrud) DeleteTask(ctx context.Context, in *DeleteTaskRequest) (*empty.Empty, error) {
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.DeleteTaskBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	if err := s.store.DeleteTask(ctx, TaskIdEq{Id: in.Id}); err != nil {
		return nil, err
	}

	return &empty.Empty{}, nil
}

func (s *TasksServiceTaskServerCrud) BatchGetTask(ctx context.Context, in *BatchGetTaskRequest) (*BatchGetTaskResponse, error) {
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.BatchGetTaskBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	getIds := make([]string, 0, len(in.Ids))
	for _, id := range in.Ids {
		getIds = append(getIds, idutil.GetId(id))
	}

	mask := s.GetViewMask(in.ViewMask)

	list, err := s.store.ListTasks(ctx, mask, TaskIdIn{Id: getIds})
	if err != nil {
		return nil, err
	}

	resultMap := make(map[string]*Task, 0)
	for i, it := range list {
		_ = i

		resultMap[it.Id] = it
	}

	isGrpc := userinfo.IsGrpcCall(ctx)

	result := make([]*Task, 0, len(in.Ids))
	for _, id := range in.Ids {
		if resultMap[id] == nil && isGrpc {
			result = append(result, &Task{})
			continue
		}
		result = append(result, resultMap[id])
	}

	return &BatchGetTaskResponse{Task: result}, nil
}

func (s *TasksServiceTaskServerCrud) GetViewMask(mask *field_mask.FieldMask) []string {
	if mask == nil || mask.GetPaths() == nil {
		return []string{}
	}
	return mask.GetPaths()
}
