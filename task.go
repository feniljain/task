package tasks

//NOTE: Please ignore formatting issues, I kind of messed up my go config for vim and hence gofmt aint working, I will try to get it working asap.


import (
	"context"
	"database/sql"
	"time"

	"github.com/elgris/sqrl"
	"github.com/golang/protobuf/ptypes"
	sqlpx "github.com/srikrsna/sqlx/proto"
	cherrors "go.saastack.io/chaku/errors"
	employeepb "go.saastack.io/employee/pb"
	"go.saastack.io/idutil"
	projectpb "go.saastack.io/project/pb"
	"go.saastack.io/task/pb"
	"google.golang.org/genproto/protobuf/field_mask"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
    // Generic Error to be returned to client to hide possible sensitive information
	errInternal = status.Error(codes.Internal, "oops! Something went wrong")
    projectPrefix = (&projectpb.Project{}).GetPrefix()
    errInvalidParent = status.Error(codes.InvalidArgument, "chaku: Invalid Parent")
    errInvalidField = status.Error(codes.InvalidArgument, "chaku: Invalid Field(s)")
    errAlreadyExist = status.Error(codes.AlreadyExists, "chaku: Already Exists")
)

type tasksServer struct {
	taskStore pb.TaskStore
	taskBLoC  pb.TasksServiceTaskServerBLoC
	*pb.TasksServiceTaskServerCrud

	taskActivityStore pb.TaskActivityStore
	taskActivityBLoC  pb.TasksServiceTaskActivityServerBLoC
	*pb.TasksServiceTaskActivityServerCrud

    parentServer pb.ParentServiceClient

    db *sql.DB
}

func NewTasksServer(

	taskSt pb.TaskStore,

	taskActivitySt pb.TaskActivityStore,

    parentServer pb.ParentServiceClient,

    db *sql.DB,

) pb.TasksServer {
	r := &tasksServer{
		taskStore: taskSt,
		taskActivityStore: taskActivitySt,
        parentServer: parentServer,
        db: db,
	}

	taskSC := pb.NewTasksServiceTaskServerCrud(taskSt, r)
	r.TasksServiceTaskServerCrud = taskSC

	taskActivitySC := pb.NewTasksServiceTaskActivityServerCrud(taskActivitySt, r)
	r.TasksServiceTaskActivityServerCrud = taskActivitySC

	return r
}

// These functions represent the BLoC(Business Logical Component) of the CRUDGen Server.
// These functions will contain business logic and would be called inside the generated CRUD functions

func (s *tasksServer) CreateTaskBLoC(ctx context.Context, in *pb.CreateTaskRequest) error {
    //No need to validate fields, already done in generated code

    // TODO: Check and test project id is valid or not [X]
    // TODO: Check and test task with similar unique fields exist [X]
    // TODO: Test timestamp mechanics [X]

    //Validating prefix as project
    if idutil.GetPrefix(in.GetParent()) != projectPrefix {
        return errInvalidParent
    }

    //Validating if parent id is legit or not
    _, err := s.parentServer.ValidateParent(ctx, &pb.ValidateParentRequest{Id: idutil.GetId(in.GetParent())})
    if err != nil {
        return err
    }

    //Check if another task with similar title exist
    _, err = s.taskStore.GetTask(ctx, []string{}, pb.TaskTitleEq{Title: in.GetTask().GetTitle()})
    if err != nil {
        if err == cherrors.ErrNotFound {
            return nil
        }
        return err
    }

    //Replacing created_on field with current timestamp
    in.Task.CreatedOn = ptypes.TimestampNow()

	return errAlreadyExist
}

func (s *tasksServer) GetTaskBLoC(ctx context.Context, in *pb.GetTaskRequest) error {
    //TODO: Test it[X]
	return nil
}

func (s *tasksServer) UpdateTaskBLoC(ctx context.Context, in *pb.UpdateTaskRequest) error {
	// TODO: Check and test if unique fields do not repeat [X]
    // TODO: Check and test field Mask Validation [X]
    // TODO: Implement logging task activity on updation [X]

    //Validating fields
    if err := in.Validate(); err != nil {
        return err
    }

    //Validate Masks
    for _, m := range in.GetUpdateMask().GetPaths() {
		if !pb.ValidTaskFieldMask(m) {
			return cherrors.ErrInvalidField
		}

		if m == "id" {
			return cherrors.ErrInvalidField
		}
	}

    //Check if another task with similar title exist
    _, err := s.taskStore.GetTask(ctx, []string{}, pb.TaskTitleEq{Title: in.GetTask().GetTitle()})
    if err != nil {
        //If not found continue to upadting task alongwith other fields
        if err == cherrors.ErrNotFound {
            var taskActivities []*(pb.TaskActivity)
            taskActivities = append(taskActivities, &pb.TaskActivity{
                TaskId: in.GetTask().GetId(),
                Msg: "Task Updated",
            })

            //Creating task activity log
            if _, err := s.taskActivityStore.CreateTaskActivitys(ctx, taskActivities...); err != nil {
                return err
            }
            return nil
        }
        return err
    }

	return cherrors.ErrObjIdExist
}

func (s *tasksServer) DeleteTaskBLoC(ctx context.Context, in *pb.DeleteTaskRequest) error {
	return nil
}

func (s *tasksServer) BatchGetTaskBLoC(ctx context.Context, in *pb.BatchGetTaskRequest) error {
	return nil
}

func (s *tasksServer) ListTaskBLoC(ctx context.Context, in *pb.ListTaskRequest) (pb.TaskCondition, error) {
    //TODO: Implement and test Like search filter on fields [X]

    if err := in.Validate(); err!=nil {
        return nil, err
    }

	//Validate view mask
	for _, m := range in.GetViewMask().GetPaths() {
		if !pb.ValidTaskFieldMask(m) {
			return nil, cherrors.ErrInvalidField
		}
	}

    if idutil.GetPrefix(in.GetParent()) == projectPrefix {
        return pb.TaskParentEq{Parent: idutil.GetParent(in.GetParent())}, nil
        //condition = append(condition, pb.TaskParentEq{Parent: idutil.GetParent(in.GetParent())})
	}

    return nil, errInvalidField
}

func (s *tasksServer) CreateTaskActivityBLoC(ctx context.Context, in *pb.CreateTaskActivityRequest) error {
	return nil
}

func (s *tasksServer) GetTaskActivityBLoC(ctx context.Context, in *pb.GetTaskActivityRequest) error {
	return nil
}

// These functions are not implemented by CRUDGen, needed to be implemented

func (s *tasksServer) ListTaskByTitle(ctx context.Context, in *pb.ListTaskByTitleRequest) (*pb.ListTaskByTitleResponse, error) {
    if err := in.Validate(); err!=nil {
        return nil, err
    }

    //Validate view mask
    for _, m := range in.GetViewMask().GetPaths() {
        if !pb.ValidTaskFieldMask(m) {
            return nil, cherrors.ErrInvalidField
        }
    }

    title := in.GetTitle()

    //Checking if title is empty
    if title != "" {
        //Listing task according to title LIKE search
        tasks, err := s.taskStore.ListTasks(ctx, []string{}, pb.TaskTitleILike{Title: title})
        if err!=nil {
            return nil, err
        }

        return &pb.ListTaskByTitleResponse{Task: tasks}, nil
    }

    return nil, errInvalidField
}

func (s *tasksServer) ListTaskByStatus(ctx context.Context, in *pb.ListTaskByStatusRequest) (*pb.ListTaskByStatusResponse, error) {
    if err := in.Validate(); err!=nil {
        return nil, err
    }

    //Validate view mask
    for _, m := range in.GetViewMask().GetPaths() {
        if !pb.ValidTaskFieldMask(m) {
            return nil, cherrors.ErrInvalidField
        }
    }

    status := in.GetStatus()

    //Not checking for valid status as it is already done at proto level

    condition := pb.TaskStatusEq{Status: status}

    tasks, err := s.taskStore.ListTasks(ctx, []string{}, condition)
    if err!=nil {
        return nil, err
    }

    return &pb.ListTaskByStatusResponse{Task: tasks}, nil
}

func (s *tasksServer) GetTasksReport(ctx context.Context, in *pb.GetTasksReportRequest) (*pb.GetTasksReportResponse, error) {
	//TODO: Test it [X]
    if err := in.Validate(); err!=nil {
        return nil, err
    }

    // TODO: Convert to start and end date [X]
    //Building Query
    sq := sqrl.Select("date(task.created_on::date)", "COUNT(*)").From("saastack_task_v1.task").Where(sqrl.And{
        sqrl.GtOrEq{"task.created_on": sqlpx.Timestamp(in.GetStartTimestamp())},
        sqrl.LtOrEq{"task.created_on": sqlpx.Timestamp(in.GetEndTimestamp())},
        sqrl.Eq{"task.status": pb.Status_Completed},
    }).GroupBy("DATE(task.created_on)").OrderBy("DATE(task.created_on)")

    query, args, err := sq.PlaceholderFormat(sqrl.Dollar).ToSql()
	if err != nil {
		return nil, err
	}

	rows, err := s.db.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}

    defer rows.Close()

    reports := make([]*pb.Report, 0)

    if err := rows.Err(); err!=nil {
        return nil, err
    }

    for rows.Next() {
        report := &pb.Report{}
        var t time.Time

        //Decoding each result obtained
        if err := rows.Scan(&t, &report.TasksDone); err != nil {
            return nil, err
        }

        //Conversion to be compatible format for proto generated code
        if report.Timestamp, err = ptypes.TimestampProto(t); err != nil {
            return nil, err
        }

        reports = append(reports, report)
    }

    //OLD CODE TRIALS FOR RUNNING QUERY

    //res, err := s.taskStore.QueryRows(ctx, `
    //    SELECT created_on::date, COUNT(*)
    //    FROM saastack_task_v1.task
    //    WHERE created_on > $1 AND created_on < $2
    //    GROUP BY created_on::date
    //    ORDER BY created_on::date ASC;
    //`, []string{}, in.GetStartTimestamp().String(), in.GetEndTimestamp().String())
    //if err!=nil {
    //    return nil, err
    //}

    //var tasks []*pb.Task
    //for res.Next(ctx) {
    //    var task *pb.Task

    //    err := res.Scan(ctx, task)
    //    if err!=nil {
    //        return nil, err
    //    }

    //    tasks = append(tasks, task)
    //}

    //fmt.Println(tasks)

    return &pb.GetTasksReportResponse{Reports: reports}, nil
}

func (s *tasksServer) ListTaskByTimestamp(ctx context.Context, in *pb.ListTaskByTimestampRequest) (*pb.ListTaskByTimestampResponse, error) {
    //NOTE: Here start and end timestamp are not taken as this is a get request and hence cant be a body, usually queries are used to achieve the same, but I wasnt able to work with queries.

    if err := in.Validate(); err!=nil {
        return nil, err
    }

	//Validate view mask
	for _, m := range in.GetViewMask().GetPaths() {
		if !pb.ValidTaskFieldMask(m) {
			return nil, cherrors.ErrInvalidField
		}
	}

    condition := pb.TaskAnd{}

    //Checking for timestamp validity, isValid was being shown up in suggestions, but giving errors while running so manual check for now
    if in.GetTimestamp().String() != "" {
        condition = append(condition, pb.TaskDueTimestampGtOrEq{DueTimestamp: in.GetTimestamp()})
    }

    tasks, err := s.taskStore.ListTasks(ctx, []string{}, condition)
    if err!=nil {
        return nil, err
    }

    return &pb.ListTaskByTimestampResponse{
        Task: tasks,
    }, nil
}

func (s *tasksServer) GetTaskActivities(ctx context.Context, in *pb.GetTaskActivitiesRequest) (*pb.GetTaskActivitiesResponse, error) {
    //TODO: Test it [X]
    if err:=in.Validate(); err!=nil {
        return nil, err
    }

    taskActivities, err := s.taskActivityStore.ListTaskActivitys(ctx, []string{}, pb.TaskActivityTaskIdEq{TaskId: in.GetId()})
    if err!=nil {
        return nil, err
    }

    return &pb.GetTaskActivitiesResponse{TaskActivity: taskActivities}, nil
}

func (s *tasksServer) UpdateAssignedEmployee(ctx context.Context, in *pb.UpdateAssignedEmployeeRequest) (*pb.Task, error) {
	//TODO: Test it [X]
    if err := in.Validate(); err!=nil {
        return nil, err
    }

    //Get the corresponding task
    task, err := s.taskStore.GetTask(ctx, []string{}, pb.TaskIdEq{Id: in.GetId()})
    if err!=nil {
        return nil, err
    }

    //Validate passed employee is correct
    _, err = s.parentServer.ValidateEmployee(ctx, &pb.ValidateEmployeeRequest{Id: in.GetEmpId()})
    if err != nil {
        return nil, err
    }

    //Assigning employee id
    task.Assignee = in.GetEmpId()

    //Updating task
    err = s.taskStore.UpdateTask(ctx, task, []string{}, pb.TaskIdEq{Id:  in.GetId()})
    if err!=nil {
        return nil, err
    }

    return task, nil
}

func (s *tasksServer) ListTaskByProject(ctx context.Context, in *pb.ListTaskByProjectRequest) (*pb.ListTaskByProjectResponse, error) {
    if err := in.Validate(); err!=nil {
        return nil, err
    }

    //TODO: Implement and test main logic with join between task and task_parent table [X]
    tasks, err := s.taskStore.ListTasks(ctx, []string{}, pb.TaskFullParentEq{Parent: in.GetParent()})
    if err!=nil {
        return nil, err
    }

    return &pb.ListTaskByProjectResponse{Tasks: tasks}, nil
}

//Helper struct for working with validation using external services
type parentServiceServer struct {
	eCli employeepb.EmployeesClient
	pCli projectpb.ProjectsClient
}

//NewParentServiceServer returns a ParentServiceServer implementation with core business logic
func NewParentServiceServer(eCli employeepb.EmployeesClient, pCli projectpb.ProjectsClient) pb.ParentServiceServer {
	return &parentServiceServer{
		eCli,
        pCli,
	}
}

//ValidateParent checks for valid project ID
func (s *parentServiceServer) ValidateParent(ctx context.Context, in *pb.ValidateParentRequest) (*pb.ValidateParentResponse, error) {
     _, err := s.pCli.GetProject(ctx, &projectpb.GetProjectRequest{Id: in.GetId(), ViewMask: &field_mask.FieldMask{Paths: []string{"id"}}})
    if err!=nil {
        return nil, errInvalidParent
    }

    return &pb.ValidateParentResponse{Valid: true}, nil
}

//ValidateEmployee checks for valid employee ID
func (s *parentServiceServer) ValidateEmployee(ctx context.Context, in *pb.ValidateEmployeeRequest) (*pb.ValidateEmployeeResponse, error) {
     _, err := s.eCli.GetEmployee(ctx, &employeepb.GetEmployeeRequest{Id: in.GetId(), ViewMask: &field_mask.FieldMask{Paths: []string{"id"}}})
    if err!=nil {
        return nil, errInvalidParent
    }

    return &pb.ValidateEmployeeResponse{Valid: true}, nil
}

